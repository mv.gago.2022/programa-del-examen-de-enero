"""Programa del examen de enero """


def insertar(dato, cantidad):

    insert = f'A continuación datos a introducir en la lista: '
    print(insert)
    seguir = True
    dic = {dato: cantidad}  # RECORDAR PONER ESTE DATO FUERA DEL BUCLE PORQUE SINO NO SE ACTUALIZA

    while seguir:

            continuar = input('Desea seguir metiendo cosas a la lista?: ')

            if continuar == 'si':
                dato = input('Que quieres añadir? :')
                cantidad = input('En cuanta cantidad?: ')

                dic2 = {dato: cantidad}
                dic.update(dic2)

                print(dic)

            elif continuar == 'no':
                seguir = False

    return dic


def borrar(dic):

    borr = f'Datos que se pueden borrar de la lista: '
    print(borr)
    parar = True

    while parar:

        clave = input('Dato a eliminar: ')
        del dic[clave]
        print(dic)

        seguir = True
        while seguir:

            continuar = input(f'Desea seguir eliminando datos?: ')

            if continuar == 'si':

                clave = input('Dato a eliminar: ')
                del dic[clave]
                print(dic)

            elif continuar == 'no':
                parar = False
                seguir = False

    return dic


'''borrar(dic={'peras': 3, 'manzanas': 8, 'putas': 5})'''


def mostrar(dic):

    mostr = input(f'Desea ver los elementos de la lista?: ')

    if mostr == 'si':
        print(dic)

    if mostr == 'no':
        exit()

    return dic


def main():

    seguir = True

    while seguir:

        pregunta = input(f'(insertar) (borrar) (mostrar) (salir): ')

        if pregunta == 'insertar':

            dato = input('Que quieres añadir? :')
            cantidad = input('En cuanta cantidad?: ')

            dic = insertar(dato, cantidad)  # IMPORTANTE PORQUE CON ESTO SE GUARDA TODO EL DIC COMPLETO

        if pregunta == 'borrar':

            borrar(dic)

        if pregunta == 'mostrar':

            mostrar(dic)

        if pregunta == 'salir':

            print(f'Cerrando el programa')
            exit()


if __name__ == '__main__':
    main()
